# download backup file from s3
# command sample: aws s3 cp s3://aiqua-prd-qgraph-backup-singapore/shard-mongo/2021/03/01/13196460b2372ee5311f_web/profiles.bson.gz --profile aiqua
import os
import bson
import gzip
import pandas as pd

if __name__ == '__main__':

    temp = {}
    total = []
    device = [
        # "android",
        # "web",
        # "ios_prd",
        # "common",
        "aaid",
        # "idfa"
    ]
    year = [
        "202004",
        "202005",
        "202006",
        "202007",
        "202008",
        "202009",
        "202010",
        "202011",
        "202012",
        "202101",
        "202102",
        "202103",
        "202104",
        "202105",
        "202106",
        "202107",
        "202108",
        "202109",
    ]


# # qg_user_id / email / aaid / idfa
    def counter(date, dir, dev, type):
        if type == "qg_user_id" or type == "email":
            backup_filename = 'profiles.bson.gz'
        elif type == "aaid" or type == "idfa":
            backup_filename = 'user_details.bson.gz'

        infile_path = os.path.join('./' + dir, backup_filename)

        with gzip.open(infile_path, "rb") as f:
            counter = 0
            for data in bson.decode_file_iter(f):
                counter += 1

                if type == "qg_user_id":
                    qg_user_id = data.get('userId')
                    temp.setdefault(qg_user_id, True)
                if type == "email":
                    email = data.get('email')
                    if email:
                        temp.setdefault(email, True)
                if type == "aaid":
                    aid = data.get('advInfo', {}).get('aid')
                    temp.setdefault(aid, True)
                if type == "idfa":
                    idfa = data.get('IDFA')
                    temp.setdefault(idfa, True)

                if counter % 1000000 == 0:
                    print(counter)

        if type == "qg_user_id":
            total.append([date, "qg_user_id", dev, len(temp.keys())])
        if type == "email":
            total.append([date, "email", dev, len(temp.keys())])
        if type == "aaid":
            total.append([date, "aid", dev, len(temp.keys())])
        if type == "idfa":
            total.append([date, "IDFA", dev, len(temp.keys())])

    def csv():
        df = pd.DataFrame(total)
        df.to_csv(f'{directory[0]}_{type_check[0]}.csv')

    directory = [
        # "01_59e4ea70742cf05cec08",
        "02_13196460b2372ee5311f",
        # "03_b89ab1e8b62d2f313428"
    ]

    type_check = [
        # "qg_user_id",
        # "email",
        "aaid",
        # "idfa"
    ]

    for dev in device:
        for yyyymm in year:
            direct = directory[0]
            type = type_check[0]

            direct = f"{direct}/{yyyymm}/{dev}"
            print(direct)
            counter(yyyymm, direct, dev, type)
    csv()
